<?php

use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

// / => HomeController.index
Route::get('/', [HomeController::class, 'index']);

// /home ==> HomeController.index
Route::get('/home', [HomeController::class, 'index']);

// /home/index ==> HomeController.index
Route::get('/home/index', [HomeController::class, 'index']);

// /home/mensaje ==> HomeController.mensaje
Route::get('/home/mensaje', [HomeController::class, 'mensaje']);

// /home/listado ==> HomeController.listado
Route::get('/home/listado', [HomeController::class, 'listado']);

// /home/imagen ==> HomeController.imagen
Route::get('/home/imagen', [HomeController::class, 'imagen']);
